import 'dart:convert';

import 'package:first_app/models/item.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

class DetailItem extends StatefulWidget {
  Item selectedItem;
  DetailItem(this.selectedItem);

  @override
  _DetailItemState createState() => _DetailItemState(selectedItem);
}

class _DetailItemState extends State<DetailItem> {
  Item selectedItem;
  _DetailItemState(this.selectedItem);

  // _handleReadCheckbox(bool readStatus) {
  //   this.setState(() {
  //     selectedItem.setReadState = readStatus;
  //   });
  // }

  save() async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString('data', jsonEncode(widget.selectedItem));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(selectedItem.title),
        backgroundColor: Colors.deepPurple,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                selectedItem.title,
                style: TextStyle(fontSize: 20),
              ),
            ),
            Checkbox(
              value: selectedItem.done,
              key: Key(selectedItem.title),
              onChanged: (value) {
                setState(() {
                  selectedItem.done = value;
                  save();
                });
              },
            )
          ],
        ),
      ),
    );
  }
}
